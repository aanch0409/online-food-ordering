import React, { Component } from 'react';
import './OrderSummary.css';

class OrderSummary extends Component {

  subtractQuantiy = (event) =>{
    let itemId = event.currentTarget.id;
    let summary = this.props.ordersummary.filter(({ ItemId }) => (ItemId === itemId));
    if(summary.length > 0 && summary[0].Quantity == 1)
    {
      this.props.removefooditem(itemId)
    }
    else
    {
      this.props.subtractquantity(itemId)
    }
  }

  addQuantiy = (event) =>{
    let itemId = event.currentTarget.id;
    this.props.addquantity(itemId)
  }

  confirmorder = () =>{

  }


  returnElements = () =>{
    let HTML =[];

    if(this.props.ordersummary.length > 0)
    {
      let Sum = 0;
      this.props.ordersummary.map((item) => {
                Sum = item.Price * item.Quantity;
                HTML.push(<div className="cartItemDiv">
                            <div className="cartItemNameDiv">
                              <span> {item.ItemName} </span>
                            </div>
                            <div className="cartItemPriceDiv">
                              <span> ₹{item.Price * item.Quantity} </span>
                            </div>
                            <div className="itemButton">
                                <div className="SubtractButton" id={item.ItemId} onClick={this.subtractQuantiy}>-</div>
                                <span className="quantityText"> {item.Quantity} </span>
                                <div className="AddButton" id={item.ItemId} onClick={this.addQuantiy}>+</div>
                            </div>
                          </div>
                      )
              })

       HTML.push(<div className="subTotalDiv">
                    <span className="subTotalDivText" > SubTotal </span>
                    <div className="subTotalDivSum"> ₹{Sum} </div>
                  </div>
                )
        HTML.push(<div className="checkoutDiv">
                    <div className="checkoutDivText" onClick={this.confirmorder}> Checkout </div>
                  </div>
                )
    }
    else
    {
          HTML.push(<div className ="NoCartItems">
              <span> No Items in Cart</span>
                </div>
            )
    }
    return HTML;
  }

  render() {

    return (
      <div className="OrderSummary">
          <div className="cartHeaderText"> Cart </div>
          <div className="cartBody">
            {
              this.returnElements()
            }
          </div>
      </div>
    );
  }
}

export default OrderSummary;
