import {createStore} from 'redux'
import {syncHistoryWithStore} from 'react-router-redux'
import { withRouter } from "react-router-dom";
import { compose } from 'redux';

import rootReducer from './Reducer/rootReducer.js'
import fooditems from './data/fooditems.js'
import category from './data/Category.js'

const defaultState = {
    fooditems : fooditems,
    ordersummary : [],
    category : category
}

const store = createStore(rootReducer , defaultState)

export default withRouter(store)