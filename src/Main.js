import React, { Component } from 'react';
import './App.css';
import { Route , Link } from 'react-router-dom'
import { withRouter } from "react-router-dom";
import Header from './MainHeader';
import MenuBody from './MenuBody';
import OrderSummary from './OrderSummary'


class Main extends Component {

  componentWillMount()
  {
        let element = document.getElementById("root")
        let height = window.innerHeight - 3
        let width = window.innerWidth 
        element.style.height = height +"px"
        element.style.width = width + "px"
  }

  viewOrderSummary = () =>{
    this.props.history.push(`/order`)
  }

  render() {

    const MenuBodyconst = (props) => {
      return (
        <MenuBody 
          {...props}
        />
      );
    }

    const OrderSummaryconst = (props) => {
      return (
        <OrderSummary 
          {...props}
        />
      );
    }

    return (
      <div className="Main">
        <Header/>
        <div className="appBody">
            <div className = "mainBody">
              <Route exact path="/" render={rouecomponent => {
              return MenuBodyconst(this.props);}} ></Route>
              <Route exact path="/" render={rouecomponent => {
              return OrderSummaryconst(this.props);}} ></Route>
            </div>
        </div>
      </div>
    );
  }
}

export default withRouter((Main));
