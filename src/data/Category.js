const category =
[
  {
    "CategoryId" : "0",
    "CategoryName": "Veg Starters",
    "isSelected" : true
  },
  {
    "CategoryId" : "1",
    "CategoryName": "Pizza",
    "isSelected" : true
  },
  {
    "CategoryId" : "2",
    "CategoryName": "Burgers",
    "isSelected" : true
  },
  {
    "CategoryId" : "3",
    "CategoryName": "Pasta",
    "isSelected" : true
  }
]

export default category