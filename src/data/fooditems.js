const fooditems =
{
  "0": [
      {
        "ItemId" : "0",
        "ItemName": "Veg Thali",
        "Price": "30",
      },
      {
        "ItemId" : "1",
        "ItemName": "Paneer Tikka",
        "Price": "60"
      },
      {
        "ItemId" : "2",
        "ItemName": "Dahi Kebab",
        "Price": "80"
      }
    ],
  "1": [
      {
        "ItemId" : "4",
        "ItemName": "8 Margerita Pizza ",
        "Price": "30",
      },
      {
        "ItemId" : "5",
        "ItemName": "Vegetable Pizza",
        "Price": "60"
      },
      {
        "ItemId" : "6",
        "ItemName": "Tandoori Paneer Pizza",
        "Price": "80"
      }
    ], 
  "2": [
      {
        "ItemId" : "7",
        "ItemName": "Veggie Burger",
        "Price": "30",
      },
      {
        "ItemId" : "8",
        "ItemName": "Aloo Tikki Burger",
        "Price": "60"
      },
      {
        "ItemId" : "9",
        "ItemName": "Grilled Burger",
        "Price": "80"
      }
    ],
  "3": [
      {
        "ItemId" : "10",
        "ItemName": "Arrabbiata Penne Pasta",
        "Price": "30",
      },
      {
        "ItemId" : "11",
        "ItemName": "Creamy Cheese Pasta ",
        "Price": "60"
      },
      {
        "ItemId" : "12",
        "ItemName": "Pasta",
        "Price": "80"
      }
    ]
}

export default fooditems