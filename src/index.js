import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux'
import store , {history} from './store'
import { BrowserRouter as Router, Route, withRouter } from "react-router-dom";

ReactDOM.render(
    <Provider store = {store}>
        <Router>
              <div className = "OuterDiv">
                <Route path="/" component={App}>
                </Route>
              </div>
        </Router>
    </Provider>,
    document.getElementById('root'));

//serviceWorker.unregister();
