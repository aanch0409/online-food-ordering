import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actionCreators from './Actions/actionCreator'

import Main from './Main'

function mapStateToProps(state)
{
    return{
        fooditems : state.fooditems,
        ordersummary : state.ordersummary,
        category : state.category
    }    
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators(actionCreators, dispatch)
}

const App = connect(mapStateToProps , 
                   mapDispatchToProps)(Main)

export default App;
