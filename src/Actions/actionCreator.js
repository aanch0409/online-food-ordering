export function addfooditem(ItemId,ItemName,Price){
    return {
        type : 'ADD_FOOD_ITEM',
        ItemId,
        ItemName,
        Price
    }
}

export function removefooditem(ItemId){
    return {
        type : 'REMOVE_FOOD_ITEM',
        ItemId
    }
}

export function addquantity(ItemId){
    return {
        type : 'ADD_QUANTITY',
		ItemId
    }
}

export function subtractquantity(ItemId){
    return {
        type : 'SUBTRACT_QUANTITY',
		ItemId
    }
}

export function selectcategory(CategoryId){
    return {
        type : 'SELECT_CATEGORY',
        CategoryId
    }
}

export function unselectcategory(CategoryId){
    return {
        type : 'UNSELECT_CATEGORY',
        CategoryId
    }
}