function ordersummary(state = {} , action)
{
    switch (action.type)
        {
        	case 'ADD_FOOD_ITEM' : 
                return [ ...state,{
                ItemId : action.ItemId,
                ItemName : action.ItemName,
                Price : action.Price,
                Quantity : 1
                }]
            case 'ADD_QUANTITY' : 
                const i = action.ItemId;
                return state.map((item) => item.ItemId === i ? { ...item, Quantity : item.Quantity + 1 } : item)
            case 'SUBTRACT_QUANTITY' : 
                const j = action.ItemId;
                return state.map((item) => item.ItemId === j ? { ...item, Quantity : item.Quantity - 1 } : item)
            case 'REMOVE_FOOD_ITEM' : 
                return state.filter(({ ItemId }) => (ItemId !== action.ItemId))
            default :
                return state  
        }
  
}

export default ordersummary