import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'

import fooditems from './fooditemReducer'
import ordersummary from './orderSummaryReducer'
import category from './categoryReducer'

const rootReducer = combineReducers (
{fooditems , ordersummary , category , routing : routerReducer}
)

export default rootReducer;