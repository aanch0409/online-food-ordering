function Category(state = {} , action)
{
    switch (action.type)
        {
        	case 'UNSELECT_CATEGORY' : 
                const i = action.CategoryId;
                return state.map((category) => category.CategoryId === i ? { ...category, isSelected : false } : category)
            case 'SELECT_CATEGORY' : 
                const j = action.CategoryId;
				return state.map((category) => category.CategoryId === j ? { ...category, isSelected : true } : category)
            default :
                return state  
        }
  
}

export default Category