import React, { Component } from 'react';
import './MenuBody.css';

class MenuBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      categoryitems : this.props.category,
      items : this.props.fooditems,
      isFilter : false,
      ordersummary : this.props.ordersummary
    }
  }


  filterList = (event) => {
    let updatedList = this.props.fooditems;
    let keys = Object.keys(updatedList);
    let FinalList = {};
    keys.map((key) => {
      let list = updatedList[key]
      list = list.filter(({ ItemName }) => (ItemName.toLowerCase()).search(event.target.value.toLowerCase()) !== -1)
      if(list.length > 0)
      {
        FinalList[key] = list;
      }
    })
    
    this.setState(
      {
        items: FinalList,
        isFilter : true
      }
    );
  }

  returnElements = () => {

    let HTML =[];

    if(Object.keys(this.state.items).length > 0)
    {
        let keys = Object.keys(this.state.items);

        keys.map((key) => {
          let Category = this.props.category[key];

              if(Category !== undefined && Category.isSelected === true)
              {
                HTML.push(<div className ="categoryHeader">
                                <span> {Category.CategoryName} </span>
                              </div>
                              )
                let Food = this.state.items[Category.CategoryId]

                  Food.map((item) =>{
                        let id = Category.CategoryId + "N" + item.ItemId
                        HTML.push(<li className="list-group-item" data-category={item.ItemName} key={item.ItemName}>
                            <div className="detailMenuDiv">
                                <div className="menuNameDiv">
                                  <span> {item.ItemName} </span>
                                </div>
                                <div className="viewPrice">
                                  <span> ₹{item.Price} </span>
                                </div>
                              <div className="addItem">
                                <div className="addItemCart" id={id} onClick = {this.addToCart}> Add </div>
                              </div>
                            </div>
                          </li>
                        )  
                  })
              }
          })   
    }
    else
    {
      HTML.push(<div className ="NoFoodItems">
                  <span> No Food Items to Show </span>
                </div>
            )

    }
    return HTML;
  }

  addToCart = (event) =>{
    let value = event.currentTarget.id;
    let key = value.split('N');
    let selectedItem = this.state.items[key[0]];;
    selectedItem = selectedItem.filter(({ ItemId }) => (ItemId === key[1]))
    let ordersummary = this.props.ordersummary;
    let summary = ordersummary.filter(({ ItemId }) => (ItemId === key[1]));
    if(summary.length > 0)
    {
        this.props.addquantity(key[1])
    }
    else
    {
      this.props.addfooditem(selectedItem[0].ItemId , selectedItem[0].ItemName , selectedItem[0].Price)
    }
  }

  filterCategory = (event) =>{
    let value = event.currentTarget.value;
    if(event.currentTarget.checked === false)
    {
      let list = this.state.categoryitems;
      list = list.filter(({ CategoryId }) => (CategoryId !== value))

      this.setState({
        categoryitems : list
      })

      this.props.unselectcategory(value);
    }
    else
    {      
      let list = this.state.categoryitems;
      list = list.filter(({ isSelected }) => (isSelected !== false))

      this.setState({
        categoryitems : list
      })

      this.props.selectcategory(value);
    }

  }

  addFilter = () =>{
    let HTML =[];
    this.props.category.map((key) =>{
        HTML.push(<div>
                    <input type="checkbox" name="filterValue" value={key.CategoryId} onChange ={this.filterCategory} checked={key.isSelected}/> {key.CategoryName}
                  </div>)
    })
    return HTML;
  }

  render() {
    return (
      <div className="filter-list">
        <form className="MenuFormDiv">
          <fieldset className="form-group">
              <input type="text" className="form-control form-control-lg" placeholder="Search" onChange={this.filterList}/>
            </fieldset>
          </form>
          <div className="menuMainDiv">
            <div className="menufilterDiv">
              <span className = "menuFilterText"> Apply Filter on Food Category </span>
              <form className="filterForm">
              {
                this.addFilter()
              }
              </form>
            </div>
            <ul className="list-group">
              {
                this.returnElements()
              }
            </ul>
          </div>
      </div>
    );
  }
}

export default MenuBody;
