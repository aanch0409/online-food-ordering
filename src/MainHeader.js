import React, { Component } from 'react';
import './App.css';
import { Route , Link } from 'react-router-dom'
import { withRouter } from "react-router-dom";

class Header extends Component {

 goHome = () =>{
 	this.props.history.push(`/`)
 }

  render() {
    return (
      <div className="appHeader" onClick = {this.goHome}>
        <span className = "headerText"> Online Food Ordering </span>
      </div>
    );
  }
}

export default withRouter((Header));
